cut -d, -f1 $(dirname $0)/my_favs.txt | sort | \
dmenu -l 10 -i -b -p 'Preferiti' -sb $1 -nf $1 -nb $2 -sf $2 | \
xargs -I {} grep {} $(dirname $0)/my_favs.txt | \
cut -d, -f2 | (xargs -I {} nohup {} &>/dev/null &)
