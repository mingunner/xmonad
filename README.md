## XMonad configuration files

### Software run

- xmonad
- xmonad-contrib
- xmobar
- dmenu
- playerctl
- kitty
- picom (compton)
- xfce suite (thunar and power manager)
- qutebrowser
- udiskie
- stalonetray
- lightdm
- numlockx
- scrot
- ranger (optional)
- zathura (optiona)

### Screenshots

![Photo 1](Screenshot.png)
![Photo 2](Screenshot2.png)
<!---
### Screenshots

![Photo 1](screenshot.png)
![Photo 2](screenshot2.png)
--->
