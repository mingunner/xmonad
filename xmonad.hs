import XMonad
import XMonad.StackSet
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Layout.Spacing
import XMonad.Layout.NoBorders
import XMonad.Layout.ToggleLayouts
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig(additionalKeysP)
import System.IO
import XMonad.Util.SpawnOnce
import XMonad.Hooks.ManageHelpers

  -- If xmobar is in your $PATH, and its config is in ~/.xmobarrc, otherwise
    -- xmproc <- spawnPipe "/path/to/xmobarbinary /path/to/.xmobarrc"

main = do   
    xmproc <- spawnPipe "xmobar"
    xmonad $ docks myConfig
                   { logHook = dynamicLogWithPP myPP{ ppOutput = hPutStrLn xmproc }
                   } `additionalKeysP` myKeys
        
-- VARIABLES           

wallpaper = "~/Immagini/sfondi-xmonad/wp5046945-frozen-lake-wallpapers.jpg"

primaryColour = "#273e89"
secondaryColour = "#babbbc"
tertiaryColour = "#addcf7"

myLayout = Tall 1 (3/100) (1/2) ||| Mirror (Tall 1 (3/100) (1/2)) ||| Full
myLayoutHook = lessBorders OnlyScreenFloat $ avoidStruts $  spacingRaw False (Border 6 0 6 0) True (Border 0 6 0 6) True $ myLayout

myStartupHook :: X ()
myStartupHook = do
    spawnOnce "udiskie &"
    spawnOnce "picom --backend glx --vsync --blur-background --blur-method dual_kawase &"
    spawnOnce "xfce4-power-manager &" -- for brightness control
    spawnOnce "stalonetray &"
    spawnOnce "numlockx on &"
    spawnOnce "light-locker &"
    spawnOnce "unclutter &" -- it makes the mouse pointer disappear
    spawnOnce "nm-applet &" -- network manager indicator
    spawnOnce "kdeconnect-indicator &"
    spawnOnce "sleep 0.3; cbatticon &"
--    spawnOnce "protonvpn-cli c -f &" -- connects to the fastest server
    spawn $ "feh --bg-fill "++wallpaper++" &"

myConfig = def
           { layoutHook = myLayoutHook
           , startupHook = myStartupHook
--           , manageHook = (composeAll [ resource =? "gl" --> doFloat ]) <+> manageDocks <+> (isFullscreen --> doFullFloat) <+> manageHook defaultConfig
           , focusedBorderColor = primaryColour
           , normalBorderColor = "white"
           , terminal = "kitty"
           , modMask = mod4Mask     -- Rebind Mod to the Windows key
           }

myPP = let fullCircle x = "●"
           emptyCircle x = "○"
           fishEye x = "◉"
           renameLay :: String -> String
           renameLay x = case x of "Spacing Tall" -> "T"
                                   "Spacing Mirror Tall" -> "M"
                                   "Spacing Full" -> "F"
           renameLay x = x
       in def { ppOrder = \(ws:l:_:_) -> [l,ws]
              , ppCurrent = xmobarColor tertiaryColour "" . fullCircle
              , ppVisible = fishEye
              , ppHidden = xmobarColor "grey" "" . fullCircle
              , ppHiddenNoWindows = emptyCircle
              , ppUrgent = xmobarColor "#f9043e" "" . fullCircle
              , ppSep = xmobarColor "white" "" " | "
              , ppLayout = xmobarColor tertiaryColour "" . renameLay
              }

myKeys= [ ("M-S-z", spawn "light-locker-command -l")
        , ("M-b", spawn "qutebrowser")
        , ("M-f", spawn "thunar")
        , ("M-x", sendMessage ToggleStruts <+> toggleScreenSpacingEnabled <+> toggleWindowSpacingEnabled)
        , ("M-p", spawn $ "dmenu_run -b -p 'dmenu' -sb '"++primaryColour++"' -nb '"++secondaryColour++"' -sf '"++secondaryColour++"' -nf '"++primaryColour++"'")
        , ("M-S-p", spawn $ "~/.xmonad/scripts/favmenu.sh '"++primaryColour++"' '"++secondaryColour++"'")
        , ("<XF86AudioMute>", spawn "pamixer -t")
        , ("M-v", spawn "pavucontrol")
        , ("<XF86AudioLowerVolume>", spawn "pamixer -d 5")
        , ("<XF86AudioRaiseVolume>", spawn "pamixer -i 5")
        , ("<XF86AudioPlay>", spawn "playerctl play-pause")
        , ("<XF86AudioPrev>", spawn "playerctl previous")
        , ("<XF86AudioNext>", spawn "playerctl next")
--        , ("<XF86MonBrightnessUp>", spawn "xbacklight -inc 10")
--        , ("<XF86MonBrightnessDown>", spawn "xbacklight -dec 10")
        , ("<Print>", spawn "scrot 'Schermata_%F_%R.png' -e 'mv $f ~/Immagini/'")
        , ("C-<Print>", spawn "scrot -u 'Schermata_%F_%R.png' -e 'mv $f ~/Immagini/'")
        ]
